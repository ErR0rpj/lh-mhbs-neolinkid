import 'package:flutter/material.dart';
import 'package:informant_app/custom_app_bar.dart';

import 'routes/router.gr.dart';

class ConnectedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                color: Colors.white,
                onPressed: (){
                  Router.navigator.popUntil((route) => route.settings.name == Router.intialPage);
                },
                child: Text("Finish", style: TextStyle(color: Colors.blue),),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                "Issue Credential",style: TextStyle(color: Colors.blue, fontSize: 32),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Select an option below to begin.",style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              SizedBox(
                height: 80,
              ),
              Column(
                children: <Widget>[
                  RaisedButton(
                    color: Colors.blue,
                    onPressed: (){
                      Router.navigator.pushNamed(Router.guardianCredentialPage);

                    },
                    child: Text("Guardian ID",style: TextStyle(color: Colors.white),)
                  ),
                  SizedBox(height: 20,),
                  RaisedButton(
                    color: Colors.blue,
                    onPressed: (){
                      Router.navigator.pushNamed(Router.birthNotificationPage);
                    },
                    child: Text("Birth Notification",style: TextStyle(color: Colors.white),),
                  ),
                  SizedBox(height: 20,),
                  RaisedButton(
                    color: Colors.blue,
                    onPressed: (){
                      Router.navigator.pushNamed(Router.guardianLinkPage);
                    },
                    child: Text("Guardian Link",style: TextStyle(color: Colors.white),),
                  ),
                  SizedBox(height: 20,),
                  RaisedButton(
                    color: Colors.blue,
                    onPressed: (){
                      Router.navigator.pushNamed(Router.vaccinePage);
                    },
                    child: Text("Vaccination",style: TextStyle(color: Colors.white),),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
