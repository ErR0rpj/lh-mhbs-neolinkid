import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:informant_app/initial_page.dart';

import 'get_qr_page.dart';
import 'routes/router.gr.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.blue));

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Informant App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: InitialPage(),
        onGenerateRoute: Router.onGenerateRoute,
        initialRoute: Router.intialPage,
        navigatorKey: Router.navigator.key);
  }
}
