import 'package:flutter/material.dart';

import 'custom_app_bar.dart';
import 'routes/router.gr.dart';
import 'util/strings.dart';
import 'util/style.dart';

class GuardianLinkPage extends StatefulWidget {
  @override
  _GuardianLinkPageState createState() => _GuardianLinkPageState();
}

class _GuardianLinkPageState extends State<GuardianLinkPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Text(
                "Guardian Link Credential",
                style: TextStyle(color: Colors.blue, fontSize: 28),
              ),
              Text(
                enterDetailsBelow,
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              SizedBox(
                height: 20,
              ),
              /*listOfFields(widget.credentialType),*/
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("BND Serial number"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Guardian's Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    onTap: (){

                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Guardian's ID number"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Child's Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                ],
              ),
              RaisedButton(
                color: Colors.blue,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return AlertDialog(
                        title: Text("Confirmation", style: TextStyle(color: Colors.black),),
                        content: Text("Are the following details correct?",style: TextStyle(color: Colors.black),),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          FlatButton(
                            child: Text("Confirm",style: TextStyle(color: Colors.blue),),
                            onPressed: () {
                              Navigator.pop(context);
                              Router.navigator.pushNamed(
                                Router.sendingCredentialPage,
                                arguments: SendingCredentialPageArguments(
                                  credentialName: "Guardian Link",
                                ),
                              );
                            },
                          ),
                          FlatButton(
                            child: Text("Edit",style: TextStyle(color: Colors.black),),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                color: Colors.white,
                onPressed: () {},
                child: Text(
                  "Cancel",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
