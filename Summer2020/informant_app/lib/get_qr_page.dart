import 'package:flutter/material.dart';
import 'package:informant_app/connected_page.dart';
import 'package:informant_app/custom_app_bar.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'routes/router.gr.dart';

class GetQRPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: CustomAppBar(),
        body: Center(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                "Credential QR Code",style: TextStyle(color: Colors.blue, fontSize: 28),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                "Scan QR code with guardian's device.",style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              SizedBox(
                height: 40,
              ),
              QrImage(
                data: "1234567890",
                version: QrVersions.auto,
                size: 200.0,
              ),
              SizedBox(
                height: 40,
              ),
              RaisedButton(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return AlertDialog(
                        title: Text("Confirmation", style: TextStyle(color: Colors.black),),
                        content: Text("Does the Guardian's device show a successful connection?",style: TextStyle(color: Colors.black),),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          FlatButton(
                            child: Text("Confirm",style: TextStyle(color: Colors.blue),),
                            onPressed: () {
                              Navigator.pop(context);
                              Router.navigator.pushNamed(Router.connectedPage);
                            },
                          ),
                          FlatButton(
                            child: Text("Cancel",style: TextStyle(color: Colors.black),),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                color: Colors.blue,
                child: Text(
                  "Next",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                color: Colors.white,
                child: Text(
                  "Cancel",
                  style: TextStyle(color: Colors.blue),
                ),
              )
            ],
          ),
        ),
    );
  }
}
