import 'package:flutter/material.dart';

import 'custom_app_bar.dart';
import 'routes/router.gr.dart';

class SendingCredentialPage extends StatefulWidget {
  final String credentialName;

  const SendingCredentialPage({Key key, this.credentialName}) : super(key: key);
  @override
  _SendingCredentialPageState createState() => _SendingCredentialPageState();
}

class _SendingCredentialPageState extends State<SendingCredentialPage> {
  bool isCredentialSent = false;

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 1),(){
      setState(() {
        isCredentialSent = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: isCredentialSent?Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/success.png"),
              SizedBox(
                height: 32,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "${widget.credentialName} Credential Sent!",
                  style: TextStyle(color: Colors.blue, fontSize: 20),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                color: Colors.blue,
                onPressed: () {
                  Router.navigator.popUntil((route) => route.settings.name == Router.connectedPage);
                },
                child: Text(
                  "Done",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ):
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              SizedBox(
                height: 32,
              ),
              Text(
                "Issuing ${widget.credentialName} Credential",
                style: TextStyle(color: Colors.blue, fontSize: 20),
                maxLines: 3,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        )
      ),
    );
  }
}
