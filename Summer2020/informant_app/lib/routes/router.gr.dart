// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:informant_app/initial_page.dart';
import 'package:informant_app/get_qr_page.dart';
import 'package:informant_app/connected_page.dart';
import 'package:informant_app/guardian_credential_page.dart';
import 'package:informant_app/birth_notification_page.dart';
import 'package:informant_app/guardian_link_page.dart';
import 'package:informant_app/vaccine_page.dart';
import 'package:informant_app/sending_credential_page.dart';

class Router {
  static const intialPage = '/';
  static const getQRPage = '/get-qr-page';
  static const connectedPage = '/connected-page';
  static const guardianCredentialPage = '/guardian-credential-page';
  static const birthNotificationPage = '/birth-notification-page';
  static const guardianLinkPage = '/guardian-link-page';
  static const vaccinePage = '/vaccine-page';
  static const sendingCredentialPage = '/sending-credential-page';
  static final navigator = ExtendedNavigator();
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.intialPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => InitialPage(),
          settings: settings,
        );
      case Router.getQRPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => GetQRPage(),
          settings: settings,
        );
      case Router.connectedPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => ConnectedPage(),
          settings: settings,
        );
      case Router.guardianCredentialPage:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute<dynamic>(
          builder: (_) => GuardianCredentialPage(key: typedArgs),
          settings: settings,
        );
      case Router.birthNotificationPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => BirthNotificationPage(),
          settings: settings,
        );
      case Router.guardianLinkPage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => GuardianLinkPage(),
          settings: settings,
        );
      case Router.vaccinePage:
        return MaterialPageRoute<dynamic>(
          builder: (_) => VaccinePage(),
          settings: settings,
        );
      case Router.sendingCredentialPage:
        if (hasInvalidArgs<SendingCredentialPageArguments>(args)) {
          return misTypedArgsRoute<SendingCredentialPageArguments>(args);
        }
        final typedArgs = args as SendingCredentialPageArguments ??
            SendingCredentialPageArguments();
        return MaterialPageRoute<dynamic>(
          builder: (_) => SendingCredentialPage(
              key: typedArgs.key, credentialName: typedArgs.credentialName),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//SendingCredentialPage arguments holder class
class SendingCredentialPageArguments {
  final Key key;
  final String credentialName;
  SendingCredentialPageArguments({this.key, this.credentialName});
}
