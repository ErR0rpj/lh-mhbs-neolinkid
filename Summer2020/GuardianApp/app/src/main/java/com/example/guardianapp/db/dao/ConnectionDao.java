package com.example.guardianapp.db.dao;


import com.example.guardianapp.db.entity.Connection;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface ConnectionDao {
    @Query("SELECT * FROM connection")
    List<Connection> getAll();

    @Query("SELECT serialized FROM connection")
    List<String> getAllSerializedConnections();

    @Query("SELECT * FROM connection WHERE id = :id")
    Connection getById(int id);

    @Insert
    void insertAll(Connection... connections);

    @Update
    void update(Connection connection);

    @Delete
    void delete(Connection connection);

    @Query("SELECT EXISTS(SELECT * FROM connection WHERE (name = :connectionName))")
    boolean checkGuardianConnected(String connectionName);
}
