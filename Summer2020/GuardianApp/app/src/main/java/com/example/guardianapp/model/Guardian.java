package com.example.guardianapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.guardianapp.db.entity.Credential;

import java.util.ArrayList;

public class Guardian implements Parcelable {
    private Credential guardianCredential;
    private ArrayList<Credential> credentials;

    public Guardian(Credential guardianCredential,
            ArrayList<Credential> credentials) {
        this.guardianCredential = guardianCredential;
        this.credentials = credentials;
    }

    public Guardian() {
        this.credentials = new ArrayList<>();
    }

    protected Guardian(Parcel in) {
        guardianCredential = in.readParcelable(Credential.class.getClassLoader());
        credentials = in.createTypedArrayList(Credential.CREATOR);
    }

    public static final Creator<Guardian> CREATOR = new Creator<Guardian>() {
        @Override
        public Guardian createFromParcel(Parcel in) {
            return new Guardian(in);
        }

        @Override
        public Guardian[] newArray(int size) {
            return new Guardian[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(guardianCredential, flags);
        dest.writeTypedList(credentials);
    }

    public Credential getGuardianCredential() {
        return guardianCredential;
    }

    public void setGuardianCredential(Credential guardianCredential) {
        this.guardianCredential = guardianCredential;
    }

    public ArrayList<Credential> getCredentials() {
        return credentials;
    }

    public void setCredentials(ArrayList<Credential> credentials) {
        this.credentials = credentials;
    }

    @Override
    public String toString() {
        return "Guardian{" +
                "guardianCredential=" + guardianCredential +
                ", credentials=" + credentials +
                '}';
    }
}
