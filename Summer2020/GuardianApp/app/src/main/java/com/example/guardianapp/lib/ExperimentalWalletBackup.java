package com.example.guardianapp.lib;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import androidx.annotation.experimental.Experimental;

@Retention(RetentionPolicy.CLASS)
@Target({ElementType.METHOD})
@Experimental(level = Experimental.Level.ERROR)
public @interface ExperimentalWalletBackup {
}
