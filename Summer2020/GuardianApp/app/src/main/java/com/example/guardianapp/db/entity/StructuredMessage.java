package com.example.guardianapp.db.entity;

import com.example.guardianapp.lib.message.StructuredMessageHolder.Response;

import java.util.List;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class StructuredMessage {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "connection_id")
    public int connectionId;

    @ColumnInfo(name = "serialized")
    public String serialized;

    @ColumnInfo(name = "entry_id")
    public String entryId;

    @ColumnInfo(name = "message_id")
    public String messageId;

    @ColumnInfo(name = "question_text")
    public String questionText;

    @ColumnInfo(name = "question_detail")
    public String questionDetail;

    @ColumnInfo(name = "answers")
    public List<Response> answers;

    @ColumnInfo(name = "selected_answer")
    public String selectedAnswer;
}
