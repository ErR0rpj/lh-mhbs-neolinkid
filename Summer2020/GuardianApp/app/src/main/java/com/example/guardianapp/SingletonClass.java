package com.example.guardianapp;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.model.Guardian;
import com.example.guardianapp.ui.home.Child;

import org.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SingletonClass {
    private static final String TAG = SingletonClass.class.getName();

    private static SingletonClass mInstance;
    private Context appContext;
    private boolean inDemoMode;
    private boolean isSDKInitialized;
    private boolean isGuardianConnected;
    private boolean isDeveloperMode;
    private Guardian guardianDemo;
    private Guardian guardianLive;
    private ArrayList<Child> children;
    private ArrayList<Child> childrenLive;
    private ArrayList<Credential> CredentialList;
    private boolean noPendingCredentialOffers;
    private int acceptedCredential = 0;
    public static SimpleDateFormat df = new SimpleDateFormat("h:mm a, dd-MMM-yyyy", Locale.getDefault());

    private SingletonClass(Context context) {
        appContext = context.getApplicationContext();
        inDemoMode = true;
        isSDKInitialized = false;
        isGuardianConnected = false;
        isDeveloperMode = false;
        guardianDemo = null;
        children = new ArrayList<>();
        CredentialList = new ArrayList<>();
        noPendingCredentialOffers = true;
        childrenLive = new ArrayList<>();
    }

    public static synchronized SingletonClass getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SingletonClass(context);
        }
        return mInstance;
    }

    public void restartDemo() {
        inDemoMode = true;
        isGuardianConnected = false;
        guardianDemo = null;
        children = new ArrayList<>();
        CredentialList = new ArrayList<>();
        noPendingCredentialOffers = true;
        acceptedCredential = 0;
    }

    public void acceptedCredentialOffer(int position, Credential credential) {
        credential.setAccepted(true);
        credential.setNeedAction(false);
        setNoPendingCredentialOffers(true);
        getCredentialList().set(position, credential);
        acceptedCredential++;
    }

    public void setGuardianCredential(Credential credential) {
        this.guardianDemo = new Guardian(credential,new ArrayList<>());
    }

    public Guardian getGuardianLive() {
        return guardianLive;
    }

    public void setGuardianLive(Guardian guardianLive) {
        this.guardianLive = guardianLive;
    }

    public void refreshButtonClicked() {
        String filename;
        String title = null;
        String type = null;
        String organization = appContext.getString(R.string.DCR);
        switch (acceptedCredential) {
            case 0:
                filename = "dummy_guardian_credential1.json";
                title = appContext.getString(R.string.guardian_id_check_credential);
                type = "GUARDIAN_CREDENTIAL";
                break;
            case 1:
                filename = "dummy_child_credential1.json";
                title = appContext.getString(R.string.birth_notification_credential);
                type = "BIRTH_NOTIFICATION_CREDENTIAL1";
                break;
            case 2:
                filename = "dummy_guardian_child_credential1.json";
                title = appContext.getString(R.string.guardian_link_credential);
                type = "GUARDIAN_LINK_CREDENTIAL1";
                break;
            case 3:
                filename = "dummy_child_immunization_credential1.json";
                title = appContext.getString(R.string.bcg_vaccination_credential);
                type = "VACCINE_CREDENTIAL1";
                organization = appContext.getString(R.string.ministry_of_health);
                break;
            case 4:
                filename = "dummy_child_credential2.json";
                title = appContext.getString(R.string.birth_notification_credential);
                type = "BIRTH_NOTIFICATION_CREDENTIAL2";
                break;
            case 5:
                filename = "dummy_guardian_child_credential2.json";
                title = appContext.getString(R.string.guardian_link_credential);
                type = "GUARDIAN_LINK_CREDENTIAL2";
                break;
            case 6:
                filename = "dummy_child_credential3.json";
                title = appContext.getString(R.string.birth_notification_credential);
                type = "BIRTH_NOTIFICATION_CREDENTIAL3";
                break;
            case 7:
                filename = "dummy_guardian_child_credential3.json";
                title = appContext.getString(R.string.guardian_link_credential);
                type = "GUARDIAN_LINK_CREDENTIAL3";
                break;
            default:
                filename = null;

        }
        if (filename != null) {
            Toast.makeText(appContext, String.format("Received %s", title), Toast.LENGTH_SHORT).show();
            JSONObject jsonObject = SingletonClass.readJson(appContext, filename);
            long time = System.currentTimeMillis();

            Credential credential = new Credential();
            credential.setDemoCredential(title, organization, time,
                    df.format(new Date(time)), type, jsonObject.toString(), jsonObject.length(),
                    false, true);
            SingletonClass.getInstance(appContext).setNoPendingCredentialOffers(false);
            SingletonClass.getInstance(appContext).getCredentialList().add(credential);

        } else {
            Toast.makeText(appContext, R.string.no_more_creds_in_demo,
                    Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isInDemoMode() {
        return inDemoMode;
    }

    public void setInDemoMode(boolean inDemoMode) {
        this.inDemoMode = inDemoMode;
    }

    public boolean isSDKInitialized() {
        return isSDKInitialized;
    }

    public void setSDKInitialized(boolean SDKInitialized) {
        isSDKInitialized = SDKInitialized;
    }

    public boolean isGuardianConnected() {
        return isGuardianConnected;
    }

    public void setGuardianConnected(boolean guardianConnected) {
        isGuardianConnected = guardianConnected;
    }

    public boolean isDeveloperMode() {
        return isDeveloperMode;
    }

    public void setDeveloperMode(boolean developerMode) {
        isDeveloperMode = developerMode;
    }

    public Guardian getGuardianDemo() {
        return guardianDemo;
    }

    public void setGuardianDemo(Guardian guardian) {
        this.guardianDemo = guardian;
    }

    public ArrayList<Child> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Child> children) {
        this.children = children;
    }

    public ArrayList<Credential> getCredentialList() {
        return CredentialList;
    }

    public void setCredentialList(
            ArrayList<Credential> credentialList) {
        CredentialList = credentialList;
    }

    public boolean isNoPendingCredentialOffers() {
        return noPendingCredentialOffers;
    }

    public void setNoPendingCredentialOffers(boolean noPendingCredentialOffers) {
        this.noPendingCredentialOffers = noPendingCredentialOffers;
    }

    public int getAcceptedCredential() {
        return acceptedCredential;
    }

    public void setAcceptedCredential(int acceptedCredential) {
        this.acceptedCredential = acceptedCredential;
    }

    public ArrayList<Child> getChildrenLive() {
        return childrenLive;
    }

    public void setChildrenLive(ArrayList<Child> childrenLive) {
        this.childrenLive = childrenLive;
    }

    public static JSONObject readJson(Context context, String file) {
        JSONObject jsonObject = null;
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonObject = new JSONObject(new String(buffer, StandardCharsets.UTF_8));
        } catch (Exception e) {
            Log.d(e.toString(), e.getMessage());
        }
        return jsonObject;
    }

    @Override
    public String toString() {
        return "SingletonClass{" +
                "appContext=" + appContext +
                ", inDemoMode=" + inDemoMode +
                ", isSDKInitialized=" + isSDKInitialized +
                ", isGuardianConnected=" + isGuardianConnected +
                ", isDeveloperMode=" + isDeveloperMode +
                ", guardian=" + guardianDemo +
                ", children=" + children +
                ", CredentialList=" + CredentialList +
                ", noPendingCredentialOffers=" + noPendingCredentialOffers +
                ", acceptedCredential=" + acceptedCredential +
                ", df=" + df +
                '}';
    }
}
