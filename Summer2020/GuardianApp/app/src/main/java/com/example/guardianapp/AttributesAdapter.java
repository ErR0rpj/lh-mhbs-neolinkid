package com.example.guardianapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.example.guardianapp.databinding.ItemAttributeBinding;
import com.example.guardianapp.model.Attribute;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;


public class AttributesAdapter extends RecyclerView.Adapter<AttributesAdapter.ViewHolder> {

    private List<Attribute> mAttributesList;
    private Context mContext;

    // Pass in the contact array into the constructor
    public AttributesAdapter(List<Attribute> attributesList, Context context) {
        this.mAttributesList = attributesList;
        this.mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // implemented view binding https://stackoverflow.com/questions/60313719/how-to-use-android-view-binding-with-recyclerview
        private ItemAttributeBinding mAttributeBinding;

        public ViewHolder(ItemAttributeBinding itemAttributeBinding) {
            super(itemAttributeBinding.getRoot());
            mAttributeBinding = itemAttributeBinding;
        }
    }

    @NotNull
    @Override
    public AttributesAdapter.ViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(ItemAttributeBinding.inflate(inflater,parent,false));
    }

    @Override
    public void onBindViewHolder(AttributesAdapter.ViewHolder viewHolder, int position) {
        Attribute attribute = mAttributesList.get(position);

        viewHolder.mAttributeBinding.tvLabel.setText(attribute.getLabel());
        viewHolder.mAttributeBinding.tvValue.setText(attribute.getValue());

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mAttributesList.size();
    }
}



