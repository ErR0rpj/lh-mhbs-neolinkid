package com.example.guardianapp.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guardianapp.CredentialOffersViewModel;
import com.example.guardianapp.GuardianActivity;
import com.example.guardianapp.R;
import com.example.guardianapp.SingletonClass;
import com.example.guardianapp.databinding.FragmentHomeBinding;
import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.model.Guardian;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

public class HomeFragment extends Fragment {
    private static String TAG = HomeFragment.class.getName();

    private HomeViewModel mHomeViewModel;
    private ChildAdapter mChildAdapter;
    private FragmentHomeBinding mHomeBinding;
    private CredentialOffersViewModel model;

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        mHomeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        // view binding
        mHomeBinding = FragmentHomeBinding.inflate(inflater, container, false);
        return mHomeBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(CredentialOffersViewModel.class);
        mHomeBinding.rvChildListDashboard.setHasFixedSize(false);
        mHomeBinding.rvChildListDashboard.setLayoutManager(new LinearLayoutManager(getContext()));

        // navigate to Guardian activity
        mHomeBinding.civGuardianHomeFragment.setOnClickListener(
                v -> startActivity(
                        new Intent(mHomeBinding.getRoot().getContext(), GuardianActivity.class)));

        SingletonClass singletonClass = SingletonClass.getInstance(getContext());
        if(singletonClass.isInDemoMode()) {

        } else {
            model.getCredentialOffers().observe(getViewLifecycleOwner(),
                    data -> {
                        Log.d(TAG, "observe-data" + data.size());
                        for (Credential credential: data){
                            if(credential.name.equalsIgnoreCase("Guardian Identity")){
                                Guardian guardian = new Guardian(credential, new ArrayList<>());
                                singletonClass.setGuardianLive(guardian);
                            }
                        }
                        setUI(singletonClass.getGuardianLive(),singletonClass.getChildren());

                    });
        }

    }


    public void init() {
        SingletonClass singletonClass = SingletonClass.getInstance(getContext());

        if(singletonClass.isInDemoMode()) {
            Guardian guardian = singletonClass.getGuardianDemo();
            if (singletonClass.isGuardianConnected()) {
                setUI(guardian,singletonClass.getChildren());

            } else { // guardian is not connected
                mHomeBinding.tvGuardianNameHomeFragment.setText(getString(R.string.scan_qr_to_connect));
                mHomeBinding.civGuardianHomeFragment.setVisibility(View.GONE);
            }
        } else {
            //model.loadCredentialOffers();
        }

    }

    public void setUI(Guardian guardian, ArrayList<Child> children){
        if (guardian == null) { // guardian is connected, but guardian identity credential is absent
            mHomeBinding.tvGuardianNameHomeFragment.setText(
                    getString(R.string.no_credential_received));
            mHomeBinding.civGuardianHomeFragment.setVisibility(View.GONE);
        } else { // guardian is connected and guardian identity credential is present
            try {
                JSONObject jsonObject = new JSONObject(
                        guardian.getGuardianCredential().getAttributes());
                Log.d(TAG, jsonObject.toString());
                try {
                    mHomeBinding.tvGuardianNameHomeFragment.setText(
                            jsonObject.getString(getString(R.string.guardian_first_name)));
                } catch (JSONException e) {
                    mHomeBinding.tvGuardianNameHomeFragment.setText(
                            jsonObject.getString("firstName"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (children.size() == 0) { // no child credential present
            mHomeBinding.tvNoChildCredentialHome.setVisibility(View.VISIBLE);
        } else { // child credentials present
            mHomeBinding.tvNoChildCredentialHome.setVisibility(View.GONE);
            mChildAdapter = new ChildAdapter(children,
                    getContext()); // initialise the adapter with child credentials
            mHomeBinding.rvChildListDashboard.setAdapter(
                    mChildAdapter); // assign recycler view with the adapter
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }
}