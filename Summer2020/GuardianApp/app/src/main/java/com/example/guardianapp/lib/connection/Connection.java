package com.example.guardianapp.lib.connection;

public interface Connection {
    String getConnectionType();
}
