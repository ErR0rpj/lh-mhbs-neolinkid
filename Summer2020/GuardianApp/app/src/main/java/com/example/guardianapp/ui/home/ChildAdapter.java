package com.example.guardianapp.ui.home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.guardianapp.ChildActivity;
import com.example.guardianapp.databinding.ItemChildBinding;
import com.example.guardianapp.R;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ViewHolder> {

    private List<Child> mChildList;
    private Context mContext;

    // Pass in the contact array into the constructor
    public ChildAdapter(List<Child> mChildList, Context context) {
        this.mChildList = mChildList;
        this.mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemChildBinding mItemChildBinding;

        public ViewHolder(ItemChildBinding childBinding) {
            super(childBinding.getRoot());
            mItemChildBinding = childBinding;

        }
    }

    @NotNull
    @Override
    public ChildAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new ViewHolder(ItemChildBinding.inflate(inflater,parent,false));
    }

    @Override
    public void onBindViewHolder(ChildAdapter.ViewHolder viewHolder, int position) {
        Child child = mChildList.get(position);

        // Set item views based on your views and data model
        try {
            viewHolder.mItemChildBinding.tvItemChildName.setText(
                    child.getChildCredential().getJSONObject().getString(
                            mContext.getString(R.string.child_first_name)));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        viewHolder.mItemChildBinding.clItemChild.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, ChildActivity.class);
            intent.putExtra(mContext.getString(R.string.child), child);
            mContext.startActivity(intent);
        });

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mChildList.size();
    }
}
