package com.example.guardianapp;

import static com.example.guardianapp.lib.connection.ConnectionCreateResult.SUCCESS;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.example.guardianapp.databinding.ActivityQrscannerBinding;
import com.example.guardianapp.db.Database;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

public class QRScannerActivity extends BaseActivity {

    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    private static final String TAG = QRScannerActivity.class.getName();
    public static final int SCAN = 1;
    private ActivityQrscannerBinding mQrscannerBinding;
    private Database db;
    private ConnectionsViewModel model;
    private String qrCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQrscannerBinding = ActivityQrscannerBinding.inflate(getLayoutInflater());
        setContentView(mQrscannerBinding.getRoot());

        model = new ViewModelProvider(this).get(ConnectionsViewModel.class);
        initViews();

    }

    private void initViews() {
        mQrscannerBinding.ibBackScanner.setOnClickListener(v -> finish());
    }

    private void initialiseDetectorsAndSources() {

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true)
                .build();

        if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            initializationOnPermissionGranted();
        } else {
            ActivityCompat.requestPermissions(QRScannerActivity.this, new
                    String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }

    }

    public void initializationOnPermissionGranted() {
        mQrscannerBinding.svScanner.setVisibility(View.VISIBLE);
        mQrscannerBinding.svScanner.getHolder().addCallback(new SurfaceHolder.Callback() {
            @SuppressLint("MissingPermission")
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    cameraSource.start(holder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                Toast.makeText(getApplicationContext(),
                        getString(R.string.qr_stopped),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0 && qrCode ==null) {
                    qrCode = barcodes.valueAt(0).displayValue;
                    SingletonClass singletonClass = SingletonClass.getInstance(getApplicationContext());
                    Log.d(TAG,"singleton:"+singletonClass.toString());
                    if(singletonClass.isInDemoMode()){
                        runOnUiThread(() -> {
                            mQrscannerBinding.rlScan.setVisibility(View.GONE);
                            mQrscannerBinding.clLoadingAfterScan.setVisibility(View.VISIBLE);

                            new Handler().postDelayed(() -> {
                                Intent intent=new Intent();
                                intent.putExtra(getString(R.string.qr_value),SUCCESS);
                                setResult(SCAN,intent);
                                finish();
                            }, 2000);

                        });

                    } else {
                        runOnUiThread(() -> {
                            mQrscannerBinding.rlScan.setVisibility(View.GONE);
                            mQrscannerBinding.clLoadingAfterScan.setVisibility(View.VISIBLE);
                            performNewConnection(qrCode);
                        });
                        //makeConnection(code);
                    }
                }
            }
        });
    }

    public void performNewConnection(String invite) {
        model.newConnection(invite).observeOnce(this, status -> {
            Intent intent=new Intent();
            intent.putExtra(getString(R.string.qr_value),status);
            setResult(SCAN,intent);
            finish();
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                if (grantResults.length == 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initializationOnPermissionGranted();
                } else {
                    finish();
                }
                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        db = Database.getInstance(getApplicationContext());
        initialiseDetectorsAndSources();
    }

    private String parseInvite(String invite) {
        if (URLUtil.isValidUrl(invite)) {
            Uri uri = Uri.parse(invite);
            String param = uri.getQueryParameter("c_i");
            if (param != null) {
                return new String(Base64.decode(param, Base64.NO_WRAP));
            } else {
                return "";
            }
        } else {
            return invite;
        }
    }

    private ConnDataHolder extractDataFromInvite(String invite) {
        try {
            JSONObject json = new JSONObject(invite);
            if (json.has("label")) {
                String label = json.getString("label");
                return new ConnDataHolder(label, null);
            }
            JSONObject data = json.optJSONObject("s");
            if (data != null) {
                return new ConnDataHolder(data.getString("n"), data.getString("l"));
            } else {
                // workaround in case details missing
                String sourceId = json.getString("id");
                return new ConnDataHolder(sourceId, null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    static class ConnDataHolder {
        String name;
        String logo;

        public ConnDataHolder(String name, String logo) {
            this.name = name;
            this.logo = logo;
        }
    }
}
