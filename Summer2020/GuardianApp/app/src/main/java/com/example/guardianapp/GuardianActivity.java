package com.example.guardianapp;

import android.content.Intent;
import android.os.Bundle;

import com.example.guardianapp.databinding.ActivityGuardianBinding;
import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.interfaces.CredentialInterface;
import com.example.guardianapp.model.Guardian;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

public class GuardianActivity extends AppCompatActivity implements CredentialInterface {

    private static String TAG = GuardianActivity.class.getName();
    private ArrayList<Credential> mCredentialArrayList;
    private CredentialAdapter mCredentialAdapter;
    private Guardian guardian;

    private ActivityGuardianBinding mGuardianBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGuardianBinding = ActivityGuardianBinding.inflate(getLayoutInflater());
        setContentView(mGuardianBinding.getRoot());


        mGuardianBinding.ibBack.setOnClickListener(v -> finish());
        mGuardianBinding.rvGuardian.setHasFixedSize(false);
        mGuardianBinding.rvGuardian.setLayoutManager(new LinearLayoutManager(this));

        if (SingletonClass.getInstance(getApplicationContext()).isInDemoMode()) {
            guardian = SingletonClass.getInstance(getApplicationContext()).getGuardianDemo();
        } else {
            guardian = SingletonClass.getInstance(getApplicationContext()).getGuardianLive();
        }
        try {
            JSONObject jsonObject = new JSONObject(
                    guardian.getGuardianCredential().getAttributes());
            try {
                mGuardianBinding.tvGuardianName.setText(
                        jsonObject.getString("Guardian First Name"));
            } catch (JSONException e) {
                mGuardianBinding.tvGuardianName.setText(
                        jsonObject.getString("firstName"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mCredentialArrayList = new ArrayList<>();
        mCredentialArrayList.add(guardian.getGuardianCredential());
        mCredentialArrayList.addAll(guardian.getCredentials());
        mCredentialAdapter = new CredentialAdapter(mCredentialArrayList, this, 0);
        mGuardianBinding.rvGuardian.setAdapter(mCredentialAdapter);


    }

    @Override
    public void goToCredential(Credential credential) {
        Intent intent = new Intent(this, CredentialActivity.class);
        intent.putExtra(getString(R.string.credential_offer), credential);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
        //overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_down);

    }
}