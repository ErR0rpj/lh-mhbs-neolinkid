package com.example.guardianapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.guardianapp.databinding.ActivityDashboardBinding;
import com.example.guardianapp.lib.connection.ConnectionCreateResult;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class DashboardActivity extends BaseActivity {

    public static final int SCAN = 1;
    public static final String TAG = DashboardActivity.class.getName();

    private ActivityDashboardBinding mDashboardBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // view binding
        mDashboardBinding = ActivityDashboardBinding.inflate(getLayoutInflater());
        setContentView(mDashboardBinding.getRoot());

        // set up bottom navigation
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(mDashboardBinding.navView, navController);

        // navigate to Scanning QR code screen
        mDashboardBinding.imgBtnScanDashboard.setOnClickListener(
                v -> startActivityForResult(new Intent(DashboardActivity.this, QRScannerActivity.class),
                        SCAN));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == SCAN) { // result received from QR Scanner Activity
                ConnectionCreateResult message = (ConnectionCreateResult) data.getExtras().get(getString(R.string.qr_value));
                Log.d(TAG, "QR::" + message);

                switch (message) {
                    case SUCCESS:
                    case REDIRECT: {
                        SingletonClass singletonClass = SingletonClass.getInstance(getApplicationContext());
                        if(singletonClass.isInDemoMode()){
                            singletonClass.setGuardianConnected(true);
                        }
                        Toast.makeText(this,"Guardian Connected. Click on Refresh",Toast.LENGTH_LONG).show();
                        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.navigation_notifications);
                        break;
                    }
                    case FAILURE:{
                        Toast.makeText(this,"Error in Guardian Connection",Toast.LENGTH_LONG).show();
                        break;
                    }
                }
            }
        }
    }
}