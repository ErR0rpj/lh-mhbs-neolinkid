package com.example.guardianapp.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class Attribute implements Parcelable {

    private String label;
    private String value;

    public Attribute(String label, String value) {
        this.label = label;
        this.value = value;
    }

    protected Attribute(Parcel in) {
        label = in.readString();
        value = in.readString();
    }

    public static final Creator<Attribute> CREATOR = new Creator<Attribute>() {
        @Override
        public Attribute createFromParcel(Parcel in) {
            return new Attribute(in);
        }

        @Override
        public Attribute[] newArray(int size) {
            return new Attribute[size];
        }
    };

    public static ArrayList<Attribute> createAttributesList(int numAttributes) {
        ArrayList<Attribute> attributeArrayList = new ArrayList<Attribute>();

        for (int i = 1; i <= numAttributes; i++) {
            attributeArrayList.add(new Attribute("Attribute Label " + i,"Value "+i));
        }

        return attributeArrayList;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(label);
        dest.writeString(value);
    }
}
