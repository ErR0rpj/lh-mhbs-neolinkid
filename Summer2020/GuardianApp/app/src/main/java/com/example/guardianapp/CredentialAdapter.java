package com.example.guardianapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.guardianapp.databinding.ItemCredentialBinding;
import com.example.guardianapp.interfaces.CredentialInterface;
import com.example.guardianapp.db.entity.Credential;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class CredentialAdapter extends RecyclerView.Adapter<CredentialAdapter.ViewHolder> {

    private List<Credential> mCredentialList;
    private Context mContext;
    private CredentialInterface mCredentialInterface;

    // Pass in the contact array into the constructor
    public CredentialAdapter(List<Credential> credentialList, Context context,
            int guardianOrChild) {
        this.mCredentialList = credentialList;
        this.mContext = context;
        if (guardianOrChild == 0) {
            this.mCredentialInterface = (GuardianActivity) mContext;
        } else {
            this.mCredentialInterface = (ChildActivity) mContext;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemCredentialBinding mCredentialBinding;

        public ViewHolder(ItemCredentialBinding itemCredentialBinding) {
            super(itemCredentialBinding.getRoot());
            mCredentialBinding = itemCredentialBinding;
        }
    }

    @NotNull
    @Override
    public CredentialAdapter.ViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new ViewHolder(ItemCredentialBinding.inflate(inflater,parent,false));
    }

    @Override
    public void onBindViewHolder(CredentialAdapter.ViewHolder viewHolder, int position) {
        Credential credential = mCredentialList.get(position);

        viewHolder.mCredentialBinding.tvCredentialTitle.setText(credential.getName());
        viewHolder.mCredentialBinding.tvType.setText(credential.getDateTimeString());
        viewHolder.mCredentialBinding.tvTime.setVisibility(View.GONE);
        viewHolder.mCredentialBinding.tvNumberOfAttributes.setText(
                String.format("%d Attributes", credential.getNumberOfAttributes()));
        viewHolder.mCredentialBinding.btnView.setOnClickListener(
                v -> mCredentialInterface.goToCredential(credential));

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mCredentialList.size();
    }
}