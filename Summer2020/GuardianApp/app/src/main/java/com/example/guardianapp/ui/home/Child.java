package com.example.guardianapp.ui.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.guardianapp.db.entity.Credential;

import java.util.ArrayList;

public class Child implements Parcelable {
    private String name;
    private String imagePath;
    private Credential childCredential;
    private ArrayList<Credential> credentials;

    public Child(Credential childCredential,
            ArrayList<Credential> credentials) {
        this.name = name;
        this.imagePath = imagePath;
        this.childCredential = childCredential;
        this.credentials = credentials;
    }

    protected Child(Parcel in) {
        name = in.readString();
        imagePath = in.readString();
        childCredential = in.readParcelable(Credential.class.getClassLoader());
        credentials = in.createTypedArrayList(Credential.CREATOR);
    }

    public static final Creator<Child> CREATOR = new Creator<Child>() {
        @Override
        public Child createFromParcel(Parcel in) {
            return new Child(in);
        }

        @Override
        public Child[] newArray(int size) {
            return new Child[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Credential getChildCredential() {
        return childCredential;
    }

    public void setChildCredential(Credential childCredential) {
        this.childCredential = childCredential;
    }

    public ArrayList<Credential> getCredentials() {
        return credentials;
    }

    public void setCredentials(ArrayList<Credential> credentials) {
        this.credentials = credentials;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(imagePath);
        dest.writeParcelable(childCredential, flags);
        dest.writeTypedList(credentials);
    }
}
