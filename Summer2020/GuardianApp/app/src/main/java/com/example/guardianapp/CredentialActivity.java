package com.example.guardianapp;

import android.os.Bundle;
import android.util.Log;

import com.example.guardianapp.databinding.ActivityCredentialBinding;
import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.model.Attribute;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

public class CredentialActivity extends AppCompatActivity {

    private static String TAG = CredentialActivity.class.getName();
    private ActivityCredentialBinding mCredentialBinding;
    private Credential credential;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCredentialBinding = ActivityCredentialBinding.inflate(getLayoutInflater());
        setContentView(mCredentialBinding.getRoot());

        mCredentialBinding.ibClose.setOnClickListener(v -> finish());

        mCredentialBinding.rvAttributes.setHasFixedSize(false);
        mCredentialBinding.rvAttributes.setLayoutManager(new LinearLayoutManager(this));


        credential = Objects.requireNonNull(getIntent().getExtras()).getParcelable(
                getString(R.string.credential_offer));
        Log.d(TAG, credential.toString() + "!");

        mCredentialBinding.tvTitle.setText(credential.getName());
        mCredentialBinding.tvOrganizationName.setText(credential.getOrganizationName());
        mCredentialBinding.tvDatetime.setText(credential.getDateTimeString());
        Iterator<?> keys = credential.getJSONObject().keys();
        ArrayList<Attribute> attributes = new ArrayList<>();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            try {
                Attribute attribute = new Attribute(key,
                        credential.getJSONObject().get(key).toString());
                attributes.add(attribute);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        AttributesAdapter credentialAdapter = new AttributesAdapter(attributes, this);
        mCredentialBinding.rvAttributes.setAdapter(credentialAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }
}