package com.example.guardianapp.ui.notifications;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.guardianapp.NotificationDetailActivity;
import com.example.guardianapp.R;
import com.example.guardianapp.databinding.ItemNotificationBinding;
import com.example.guardianapp.db.entity.Credential;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private List<Credential> mCredentialList;
    private Context mContext;

    // Pass in the contact array into the constructor
    public NotificationAdapter(List<Credential> credentials, Context context) {
        this.mCredentialList = credentials;
        this.mContext = context;
    }

    public void setCredentials(List<Credential> credentialList) {
        this.mCredentialList = credentialList;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ItemNotificationBinding mNotificationBinding;

        public ViewHolder(ItemNotificationBinding itemNotificationBinding) {
            super(itemNotificationBinding.getRoot());
            mNotificationBinding = itemNotificationBinding;
        }
    }

    @NotNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        return new ViewHolder(ItemNotificationBinding.inflate(inflater,parent,false));
    }

    @Override
    public void onBindViewHolder(@NotNull final NotificationAdapter.ViewHolder viewHolder,
            final int position) {
        Credential credential = mCredentialList.get(position);
        Log.d("NotifAdapter",credential.toString());
        String result;
        if (credential.getNeedAction() == null || credential.getNeedAction()) {
            viewHolder.mNotificationBinding.ivNotificationImage.setImageDrawable(
                    mContext.getDrawable(R.drawable.ic_baseline_error_24_blue));
            result = mContext.getString(R.string.pending);
        } else {
            if (credential.getAccepted()) {
                viewHolder.mNotificationBinding.ivNotificationImage.setImageDrawable(
                        mContext.getDrawable(R.drawable.ic_baseline_check_24_green));
                result = mContext.getString(R.string.accepted);
            } else {
                viewHolder.mNotificationBinding.ivNotificationImage.setImageDrawable(
                        mContext.getDrawable(R.drawable.ic_baseline_clear_24_red));
                result = mContext.getString(R.string.rejected);
            }
        }
        viewHolder.mNotificationBinding.tvNotificationTitle.setText(credential.getName());
        viewHolder.mNotificationBinding.tvNotificationType.setText(String.format("STATUS: %s", result));
        if(credential.getDateTimeString() != null) {
            viewHolder.mNotificationBinding.tvNotificationTime.setVisibility(View.VISIBLE);
            viewHolder.mNotificationBinding.tvNotificationTime.setText(credential.getDateTimeString());
        } else {
            viewHolder.mNotificationBinding.tvNotificationTime.setVisibility(View.INVISIBLE);
        }


        viewHolder.mNotificationBinding.cardItemNotification.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, NotificationDetailActivity.class);
            intent.putExtra(mContext.getString(R.string.credential_offer), credential);
            intent.putExtra(mContext.getString(R.string.position), position);
            mContext.startActivity(intent);
        });

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mCredentialList.size();
    }
}

